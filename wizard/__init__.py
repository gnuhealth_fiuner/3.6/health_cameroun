# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from .patient_create import *
from .patient_update import *
from .wizard_create_upec_report import *
from .wizard_appointment_evaluations import *
from .wizard_appointment_ambulatory_cares import *
from .wizard_appointment_lab_results import *
from .wizard_create_lab_test import *


